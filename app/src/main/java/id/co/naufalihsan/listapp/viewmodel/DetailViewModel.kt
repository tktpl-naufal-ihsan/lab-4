package id.co.naufalihsan.listapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.naufalihsan.listapp.model.Pokemon
import id.co.naufalihsan.listapp.model.pokemonDb

class DetailViewModel : ViewModel() {
    val pokemon = MutableLiveData<Pokemon>()

    fun fetch(pokemonIndex: Int) {
        pokemon.value = pokemonDb[pokemonIndex]
    }

}
