package id.co.naufalihsan.listapp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import id.co.naufalihsan.listapp.R
import id.co.naufalihsan.listapp.model.Pokemon
import kotlinx.android.synthetic.main.fragment_item.view.*
import kotlin.collections.List

class ListAdapter(val list: ArrayList<Pokemon>) :

    RecyclerView.Adapter<ListAdapter.PokemonViewHolder>() {

    fun updateList(newList: List<Pokemon>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.fragment_item, parent, false)
        return PokemonViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.view.imageView.setImageResource(list[position].image)
        holder.view.name.text = list[position].name
        holder.view.type.text = list[position].type
        holder.view.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(ListDirections.actionListToDetail().setId(position))
        }

    }

    class PokemonViewHolder(var view: View) : RecyclerView.ViewHolder(view)
}