package id.co.naufalihsan.listapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.naufalihsan.listapp.model.Pokemon
import id.co.naufalihsan.listapp.model.pokemonDb

class ListViewModel : ViewModel() {
    val pokemons = MutableLiveData<List<Pokemon>>()
    val error = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun refesh() {

        val pokemonList: ArrayList<Pokemon> = pokemonDb

        pokemons.value = pokemonList
        error.value = false
        loading.value = false
    }
}
