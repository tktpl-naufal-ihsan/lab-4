package id.co.naufalihsan.listapp.model

import id.co.naufalihsan.listapp.R

data class Pokemon(
    val id: String?,
    val name: String?,
    val type: String?,
    val weight: String?,
    val height: String?,
    val image: Int
)


val pikachu = Pokemon(
    "1",
    "Pikachu",
    "Electric",
    "6.0 kg (13.2 lbs)",
    "0.4 m (1′04″)",
    R.drawable.pikachu
)

val ditto = Pokemon(
    "2",
    "Ditto",
    "Normal",
    "4.0 kg (8.8 lbs)",
    "0.3 m (1′00″)",
    R.drawable.ditto
)

val bulbasaur = Pokemon(
    "3",
    "Bulbasaur",
    "Grass",
    "6.9 kg (15.2 lbs)",
    "0.7 m (2′04″)",
    R.drawable.bulbasaur
)

val snorlax = Pokemon(
    "4",
    "Snorlax",
    "Normal",
    "460.0 kg (1014.1 lbs)",
    "2.1 m (6′11″)",
    R.drawable.snorlax
)


val magikarp = Pokemon(
    "5",
    "Magikarp",
    "Water",
    "10.0 kg (22.0 lbs)",
    "0.9 m (2′11″)",
    R.drawable.magikarp
)

val meoweth = Pokemon(
    "5",
    "Meoweth",
    "Normal",
    "4.2 kg (9.3 lbs)",
    "0.4 m (1′04″)",
    R.drawable.meowth
)


val squirtle = Pokemon(
    "5",
    "Squirtle",
    "Water",
    "9.0 kg (19.8 lbs)",
    "0.5 m (1′08″)",
    R.drawable.squirtle
)

val pokemonDb = arrayListOf(
    pikachu, ditto, bulbasaur, snorlax, magikarp,
    meoweth, squirtle
)