package id.co.naufalihsan.listapp.view

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import id.co.naufalihsan.listapp.R
import id.co.naufalihsan.listapp.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.detail_fragment.*


class Detail : Fragment() {

    companion object {
        fun newInstance() = Detail()
    }

    private lateinit var viewModel: DetailViewModel
    private var pokemonId = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            pokemonId = DetailArgs.fromBundle(it).id
        }

        viewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        viewModel.fetch(pokemonId)

        observeViewModel()

    }

    fun observeViewModel() {
        viewModel.pokemon.observe(this, Observer { pokemon ->
            pokemon?.let {
                pokemonImage.setImageResource(pokemon.image)
                pokemonName.text = pokemon.name
                pokemonType.text = pokemon.type
                pokemonWeight.text = pokemon.weight
                pokemonHeight.text = pokemon.height
            }
        })

    }


}
